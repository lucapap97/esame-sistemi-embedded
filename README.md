# Esame Sistemi Embedded

**Braccio raccoglitore**

*Autori*: Luca Papparotto, Alessandro Varotto

descrizione:
Il progetto prevede la realizzazione di un braccio meccanico in grado di raccogliere oggetti e spostarli in un cestino secondo due modalità:
- MANUALE: il braccio viene comandato attraverso l'utilizzo di un joystick, una volta posizionato sull'oggetto e premuto il tasto del joystick il braccio automaticamente preleverà l'oggetto e lo sposterà nella zona designata.

- AUTOMATICO: il braccio meccanico attraverso il sensore ultrasonico cerca, nel range della sua presa, oggetti da poter raccogliere e buttare nel cestino.

**Hardware e materiale utilizzato:**


- Esp32: [link](https://www.amazon.it/AZDelivery-NodeMCU-Development-Arduino-gratuito/dp/B071P98VTG/ref=sr_1_4?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=esp32&qid=1562665039&s=gateway&sr=8-4)
- Servo sg90 x5 (1 x la rotazione della base, 2 x movimento braccio sull'asse delle y, 2 x la mano): [link](https://www.amazon.it/IDEAW-Servomotore-controlli-aeroplano-elicottero/dp/B07BF7B69M/ref=sr_1_9?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=servo+sg90&qid=1562665173&s=gateway&sr=8-9)
- Joystick x1: [link](https://www.amazon.it/PS2-Game-joystick-biassiale-controllo-Raspberry/dp/B06Y1HB7KR/ref=sr_1_3?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=joystick+arduino&qid=1562665273&s=gateway&sr=8-3)
- Sensore ultrasonico x1: [link](https://www.amazon.it/Bobury-ultrasonico-misurazione-distanza-Microcontroller/dp/B06XK1NBDS/ref=sr_1_7?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=sensore+ultrasonico+arduino&qid=1562665311&s=gateway&sr=8-7)
- Led x2: [link](https://www.amazon.it/Cylewet-bianca-Arduino-codice-CLW1040/dp/B06XSCGLH5/ref=sr_1_1_sspa?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=led+arduino&qid=1562665367&s=gateway&sr=8-1-spons&psc=1)
- Microswitch x1: [link](https://www.amazon.it/ROSENICE-Pezzi-Interruttore-Levetta-Micro/dp/B01JRQWCQ2/ref=sr_1_1?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=microswitch&qid=1562665431&s=gateway&sr=8-1)
- Interruttore x1: [link](https://www.amazon.it/Supmico-miniatura-interruttore-cruscotto-precipitare/dp/B07C72HDZT/ref=pd_sbs_79_8?_encoding=UTF8&pd_rd_i=B07C72HDZT&pd_rd_r=13dbadfe-a22e-11e9-95e0-012afba0f064&pd_rd_w=EtIQt&pd_rd_wg=Zqc91&pf_rd_p=37660d27-94f1-4ebe-be01-184b332a9b15&pf_rd_r=S53Q0Y8E4Z5VM7AJ48E3&psc=1&refRID=S53Q0Y8E4Z5VM7AJ48E3)
- Millefori x2
- Vari piedini maschio e femmina
- Connettore usb per alimentazione esterna 
- Alimentatore 5v 2A
- Plexiglass per la realizzazione fisica del robot

*Licenza scelta:* GPLv3

**Librerie utilizzate:**

- libreria Servo per  Esp32: [https://github.com/RoboticsBrno/ESP32-Arduino-Servo-Library/](https://github.com/RoboticsBrno/ESP32-Arduino-Servo-Library/)

**Limitazioni del Progetto**

Il nostro progetto prevede, quindi, la realizzazione di un braccio meccanico che comporta precisione quasi millimetrica al fine di ottenere buoni risultati, sepecialmente nella modalità automatica.

Essendo costruito a mano e con del plexiglass, la struttura non è delle più solide nè tantomeno delle più diritte, di conseguenza scompensi di peso e mancata perpendicolarità dei bracci implicano una non perfetta precisione del robot.

I servo inoltre non sono molto performanti e alcune volte mancano di fluidità dovuto al peso del braccio e gli attriti con i materiali utilizzati (sopratutto nella base).

Temperatura, pressione e umidità potrebbero influire sul calcolo della distanza di un oggetto tramite il sensore ultrasonico poichè la velocità di propagazione dell'onda sonora è influenzata dalle caratteristiche del fluido (in questo caso) che attraversa.

**Struttura del repository**

 - Code: contiene il software necessario al funzionamento del braccio e la libreria utilizzata per esp32
 - Documents: contiene materiale vario aggiuntivo come video di funzionamento del robot
