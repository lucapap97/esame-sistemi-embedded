/** 
 * il seguente codice rappresenta il codice utilizzato in una fase embrionale del progetto, fare riferimento a braccio_raccoglitore.ino per quello definitivo 
 * il presente codice è funzionante ma non sarà la versione definitiva in quanto va ancora ristrutturato per aumentarne la leggibilità e diminuire la ripetizione di codice
 */
#include <Servo.h>

const int trigPin = 4;
const int echoPin = 5;
long duration = 0;
int distance = 0;

int minimo = 99;
int min_degree = 90;

bool autom = false; //se e' true attiva il rilascio automatico

bool modo = true; //true=joystick, false=ultra sensore


// Arduino pin numbers
const int X_pin = 32; // pin analogico connesso al pin X del joystick
const int Y_pin = 33; // pin analogico connesso al pin Y del joystick

const int SW_pin = 15; // pin digitale connesso al pin SW del joystick
const int SWITCH_pin_2 = 18; //pin digitale connesso al microswitch posto sulla mano
const int SWITCH_MODE = 34; //pin digitale connesso al microswitch posto sulla mano

int val_base, val_braccio_1, val_braccio_2; //val_base: valore letto in corrsipondenza di X_pin
                                            //val_braccio_1, val_braccio_2 = valori letti in corrsipondenza di Y_Pin
int myspeed;                                //velocità di movimento del braccio in gradi
/*
-----------NOTA SULLA VELOCITÀ DI MOVIMENTO---------

La velocità di movimento del braccio è dato sia dal numero di gradi di spostamento del servo ad ogni iterazione
sia dalla velocità di ogni iterazione, da noi rallentata con un delay(50)
nel nostro caso lo spostamento è quindi di 2 gradi ogni 50ms (circa), per uno spostamento teorico di 40° ogni secondo
(1sec/0,05sec)*2° = 40°/sec

*/ 

int degree_base, degree_braccio_1, degree_braccio_2, degree_mano_1, degree_mano_2; //gli angoli di ogni servo

//creazione degli "oggetti" servo
Servo servo_base;  
Servo servo_braccio_1;
Servo servo_braccio_2;
Servo servo_mano_1;
Servo servo_mano_2;


void setup() {

  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input

  pinMode(SW_pin, INPUT_PULLUP);
  digitalWrite(SW_pin, HIGH);

  pinMode(SWITCH_pin_2, INPUT);
  //digitalWrite(SWITCH_pin_2, HIGH);
  
  Serial.begin(9600);

  //associazione del servo al relativo pin
  servo_base.attach(12);
  
  servo_braccio_1.attach(14);
  servo_braccio_2.attach(26);

  servo_mano_1.attach(27);
  servo_mano_2.attach(25);

  //impostazione velocità, vedi nota sopra per la determinazione della velocità di movimento del braccio
  myspeed = 2;

  //inizializzazione dei servo in posizione neutra (tutti a 90°)
  degree_base = 90 ; degree_braccio_1 = 120; degree_braccio_2 = 60; degree_mano_1 = 90; degree_mano_2 = 90;

  //spostamenti dei servo in posizione neutra 
  servo_base.write(degree_base);
  servo_braccio_1.write(degree_braccio_1);
  servo_braccio_2.write(degree_braccio_2);

  servo_mano_1.write(degree_mano_1);
  servo_mano_2.write(degree_mano_2);

 }

void loop() {

  val_base = analogRead(X_pin);
  val_braccio_1 = analogRead(Y_pin);
  val_braccio_2 = analogRead(Y_pin);

  if (analogRead(SWITCH_MODE)<4000)
    modo=true;
  else
    modo=false;

  if (modo) { // se true controllo manuale del braccio
        
        //////////////////modalità manuale/////////////////

        // ruota la base verso sx
        if (val_base < 1000 && degree_base < 150){
          degree_base = degree_base + myspeed;          
          servo_base.write(degree_base);
        }

      // ruota la base verso dx
        if (val_base > 3000 && degree_base > 0){
          degree_base = degree_base - myspeed;        
          servo_base.write(degree_base);
        }
      
      // alza il braccio
        if (val_braccio_1 < 1000 && degree_braccio_1 <= 130) {
          degree_braccio_1 = degree_braccio_1 + myspeed;   
          degree_braccio_2 = degree_braccio_2 - myspeed;
          servo_braccio_1.write(degree_braccio_1);
          servo_braccio_2.write(degree_braccio_2);
        }
      // abbassa il braccio
        if (val_braccio_1 > 3000 && degree_braccio_1 >= 80) {
          degree_braccio_1 = degree_braccio_1 - myspeed;
          degree_braccio_2 = degree_braccio_2 + myspeed;
          servo_braccio_1.write(degree_braccio_1);
          servo_braccio_2.write(degree_braccio_2);
        }

        //prendi l'oggetto

          if(digitalRead(SW_pin)==0){
          while(digitalRead(SWITCH_pin_2)==1 && degree_mano_1 <= 128){
              degree_mano_1 = degree_mano_1 + 2;
              degree_mano_2 = degree_mano_2 - 2;
              servo_mano_1.write(degree_mano_1);
              servo_mano_2.write(degree_mano_2);
              delay(50);
            }
            
      

          if (degree_mano_1 >= 128){
            degree_mano_1 = 90;
            degree_mano_2 = 90;
            servo_mano_1.write(degree_mano_1);
            servo_mano_2.write(degree_mano_2);
           }
           else{
            servo_mano_1.write(degree_mano_1+4);
            servo_mano_2.write(degree_mano_2-4);
            autom = true;
           }
          }
  }

  else{ //se modo false attivazione modalità automatica

    //////////////////modalità automatica/////////////////  

        distance = 99;
        minimo = 99;
        min_degree = 0;


      //alza il braccio per la ricerca per non far cadere gli oggetti
       while(degree_braccio_1 <= 120){
          degree_braccio_1 = degree_braccio_1 + myspeed;
          degree_braccio_2 = degree_braccio_2 - myspeed;
          servo_braccio_1.write(degree_braccio_1);
          servo_braccio_2.write(degree_braccio_2);
          delay(50);
        }

      //ruota il braccio all'estremo desto per iniziare la scansione
        while(degree_base <=130){
          degree_base = degree_base + myspeed;
          servo_base.write(degree_base);
          delay(50); // sempre per il fattore velocità di movimento
        }

       //scansiona l'ambiente fino a raggiungere i 30°
        for( ;degree_base>20; degree_base-=myspeed){
            
            digitalWrite(trigPin, LOW);
            delayMicroseconds(2);
            digitalWrite(trigPin, HIGH);
            delayMicroseconds(10);  
            digitalWrite(trigPin, LOW);
            
            // Lettura echo_pin per calcolare la distanza in tempo
            duration = pulseIn(echoPin, HIGH);
            
            // Calcolo della distanza
            if (duration<2500)  //se la durata è troppo grande esp32 fa fatica a calcolare, per evitare ciò abbiamo limitatla durata massima
              distance= duration*0.034/2;
            else distance = 99; //distanza massima fittizia

            //Se la distanza calcolata è la minima, allora l'oggetto più vicino è quello a grado deegree_base
            if(distance<minimo && (distance >= 24 && distance <=29)){
                min_degree = degree_base;
                minimo = distance;
              
            }
            servo_base.write(degree_base);
            
          delay(50); // sempre per il fattore velocità di movimento
        }


         //raccolta dell'oggetto
          if(minimo >= 24 && minimo <=29){
              // torna nella posizione dell'oggetto più vicino
                 while(degree_base <=min_degree){
                      degree_base = degree_base + myspeed;
                      servo_base.write(degree_base);
                      delay(50);
                 }


          //abbassa il braccio
              while(degree_braccio_1 >= 90){
                degree_braccio_1 = degree_braccio_1 - myspeed;
                degree_braccio_2 = degree_braccio_2 + myspeed;
                servo_braccio_1.write(degree_braccio_1);
                servo_braccio_2.write(degree_braccio_2);
                delay(50);
              }
           
           //chiude la mano finchè non prende l'oggetto
           while(digitalRead(SWITCH_pin_2)==1 && degree_mano_1 <= 128){
            degree_mano_1 = degree_mano_1 + 2;
            degree_mano_2 = degree_mano_2 - 2;
            servo_mano_1.write(degree_mano_1);
            servo_mano_2.write(degree_mano_2);
            delay(50);
           }
           //stringi ancora un po' per aumentare la presa con l'oggetto
           servo_mano_1.write(degree_mano_1+4);
           servo_mano_2.write(degree_mano_2-4);
           delay(1500);

           //attiva rilascio automatico
           if (degree_mano_1 >= 128){
            degree_mano_1 = 90;
            degree_mano_2 = 90;
            servo_mano_1.write(degree_mano_1);
            servo_mano_2.write(degree_mano_2);
           }
           else
            autom = true;
    }
           
  }
  
  ///////////////////////////////////////////////////////////////////

  //Automatic release start
   if(autom){

    //alza il braccio
        while(degree_braccio_1 <= 120){
          degree_braccio_1 = degree_braccio_1 + myspeed;
          degree_braccio_2 = degree_braccio_2 - myspeed;
          servo_braccio_1.write(degree_braccio_1);
          servo_braccio_2.write(degree_braccio_2);
          delay(50);
        }
        
       //ruotati verso il cestino
         while(degree_base >=4){
          degree_base = degree_base - myspeed;
          servo_base.write(degree_base);
          delay(50);
        }
       //apri la mano
        delay(2000);
       
        degree_mano_1 = 90;
        degree_mano_2 = 90;
        servo_mano_1.write(degree_mano_1);
        servo_mano_2.write(degree_mano_2);
        
        delay(2000);
   
       //ruotati nella posizione neutra (90°)
        while(degree_base <=90){
          degree_base = degree_base + myspeed;
          servo_base.write(degree_base);
          delay(50);
        }

     // disattiva il rilascio automatico
        autom = false;
     //resetta parametri modalita automatica
        
  }
// automatic release end


 


  Serial.print("Base: ");
  Serial.print(degree_base);
  Serial.print("- Braccio1: ");
  Serial.print(degree_braccio_1);
  Serial.print("- Braccio2: ");
  Serial.print(degree_braccio_2);
  Serial.println("- Mano1: ");
  Serial.print(degree_mano_1);
  Serial.print("- Mano2: ");
  Serial.print(degree_mano_2);
  Serial.print("- Switch1: ");
  Serial.println(digitalRead(SW_pin));
  Serial.print("- Switch2: ");
  Serial.println(digitalRead(SWITCH_pin_2));
  Serial.println("");
  Serial.println("");
//  Serial.println("----" + digitalRead(SW_pin) + " ----" );
  //Serial.print();
  //Serial.println(degree_braccio_2);

  
  
  // Prints the distance on the Serial Monitor
  Serial.print("Distance: ");
  Serial.println(distance);

  delay(50);

}
