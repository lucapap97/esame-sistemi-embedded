#include <Servo.h>

/*

 il seguente codice rappresentra la versione finale ma potrebbe comunque subire piccole variazioni per migliorare la precisione del robot.
 es.angoli potrebbero variare in fase di test.
 
 */

const int trigPin = 4;
const int echoPin = 5;
long duration = 0;
int distance = 0;
int minimo = 99;
int min_degree = 0;
bool autom = false; // se è true attiva il rilascio automatico
bool modo = true;   // true = joystick, false = sensore ultrasuoni

// Pin esp32
const int X_pin = 32;   // pin analogico connesso al pin X del joystick
const int Y_pin = 33;   // pin analogico connesso al pin Y del joystick
const int SW_pin = 15;  // pin digitale connesso al pin SW del joystick
const int SW_mano_pin = 18; // pin digitale connesso al microswitch posto sulla mano
const int SW_mode_pin = 34; // pin digitale connesso all'interruttore per cambiare modalità
                            //       |
                            //       V
                            // attualmente per problemi hardware funziona solo con pin analogico

int val_base, val_braccio; // val_base: valore letto in corrsipondenza di X_pin
                           // val_braccio = valore letto in corrsipondenza di Y_Pin
int myspeed=2;  // velocità di movimento del braccio in gradi

/*
 
 -----------NOTA SUI MOVIMENTI DEL ROBOT----------

La libreria Servo.h mette a disposizione la funzione servo.write(degree) che dato un angolo in input, muove il servo a quel determinato angolo.
Teoricamente , quindi, nel caso in cui ci trovassimo a 130° e volessimo spostarci a 30°, ci basterebbe invocare la funzione servo_da_spostare.write(30).
Facendo così però lo spostamento è pressocchè immediato, applicando quindi una rotazione molto intesa e generando un momento abbastanza elevato che può portare alla rottura del robot.

Come conseguenza abbiamo dovuto introdurre una struttura del tipo:

while(degree_attuali <= degree_da_raggiungere){
          degree_base = degree_base + myspeed;
          servo_da_muovere.write(degree_base);
          delay(50);
      }

ovvero un ciclo che aumenta/diminuisce (a seconda del verso di rotazione) i gradi del servo in modo controllato al fine di evitare brusche accelerazioni e movimenti del braccio per presernvarne l'integrità dello stesso.

/*
-----------NOTA SULLA VELOCITÀ DI MOVIMENTO----------

La velocità di movimento del braccio è dato sia dal numero di gradi di spostamento del servo ad ogni iterazione
sia dalla velocità di ogni iterazione, da noi rallentata con un delay(50)
nel nostro caso lo spostamento è quindi di 2 gradi ogni 50ms (circa), per uno spostamento teorico di 40° ogni secondo
(1sec/0,05sec)*2° = 40°/sec
*/ 

int degree_base, degree_braccio, degree_mano; //gli angoli di ogni servo

// creazione degli "oggetti" servo
Servo servo_base;  
Servo servo_braccio_1;
Servo servo_braccio_2;
Servo servo_mano_1;
Servo servo_mano_2;


void setup() {

  pinMode(trigPin, OUTPUT); // trigPin come output
  pinMode(echoPin, INPUT); // echoPin come Input
  pinMode(SW_pin, INPUT_PULLUP);
  digitalWrite(SW_pin, HIGH);
  pinMode(SW_mano_pin, INPUT);

  //pinMode(19, INPUT);
  
  // associazione del servo al relativo pin
  servo_base.attach(12); 
  servo_braccio_1.attach(14);
  servo_braccio_2.attach(26);
  servo_mano_1.attach(27);
  servo_mano_2.attach(25);

  // inizializzazione dei servo in posizione neutra (tutti a 90°)
  degree_base = 90 ; degree_braccio = 120; degree_mano = 65;

  // spostamenti dei servo in posizione neutra 
  servo_base.write(degree_base);
  servo_braccio_1.write(degree_braccio);
  servo_braccio_2.write(180-degree_braccio);
  servo_mano_1.write(degree_mano);
  servo_mano_2.write(180-degree_mano);

  Serial.begin(9600);
}


void loop() {

 
  val_base = analogRead(X_pin);
  val_braccio = analogRead(Y_pin);

  if (analogRead(SW_mode_pin) > 4000)
    modo=true;
  else
    modo=false;

  if (modo) { // se modo = true controllo manuale del braccio
        
  //////////////////modalità manuale//////////////////

        // ruota la base verso sx
        if (val_base < 1000 && degree_base < 150){
          degree_base = degree_base + myspeed;          
          servo_base.write(degree_base);
        }

        // ruota la base verso dx
        if (val_base > 3000 && degree_base > 20){
          degree_base = degree_base - myspeed;        
          servo_base.write(degree_base);
        }
      
        // alza il braccio
        if (val_braccio < 1000 && degree_braccio <= 130) {
          degree_braccio = degree_braccio + myspeed;
          servo_braccio_1.write(degree_braccio);
          servo_braccio_2.write(180-degree_braccio);
        }
        // abbassa il braccio
        if (val_braccio > 3000 && degree_braccio >= 80) {
          degree_braccio = degree_braccio - myspeed;
          servo_braccio_1.write(degree_braccio);
          servo_braccio_2.write(180-degree_braccio);
        }

        // premendo il tasto centrale del joystick inizia la raccolta dell'oggetto
        if(digitalRead(SW_pin) == 0)
          raccolta();
  }
  else{ // se modo = false controllo automatico del braccio

  //////////////////modalità automatica//////////////////

      // resetta parametri modalita automatica
      distance = 99;
      minimo = 99;
      min_degree = 0;


      // alza il braccio per non far scontrarsi con gli oggetti durante la ricerca
      muovi_braccio(120);

      // ruota il braccio all'estremo destro per iniziare la scansione
      muovi_base(130);

      // scansiona l'ambiente muovendo la base fino a raggiungere i 30°
      for( ; degree_base > 20; degree_base-=1){      
          digitalWrite(trigPin, LOW);
          delayMicroseconds(2);
          digitalWrite(trigPin, HIGH);
          delayMicroseconds(10);  
          digitalWrite(trigPin, LOW);
          
          // lettura echo_pin per calcolare la distanza in tempo
          duration = pulseIn(echoPin, HIGH);
            
          // calcolo della distanza
          if (duration<2500)  //se la durata è troppo grande esp32 fa fatica a calcolare, per evitarlo abbiamo limitato la durata massima
              distance = duration*0.034/2;
          else
              distance = 99; // distanza massima fittizia (Non è possibile prendere l'oggetto)

          // se la distanza calcolata è la minima, allora l'oggetto più vicino è quello a grado deegree_base
          if(distance<minimo && (distance >= 23 && distance <=30)){
              min_degree = degree_base;
              minimo = distance;
            
          }
          servo_base.write(degree_base); 
          delay(50); // sempre per il fattore velocità di movimento
      }

      if(minimo >= 23 && minimo <= 30){ // raccolta dell'oggetto
          muovi_base (min_degree); // torna nella posizione dell'oggetto più vicino
          muovi_braccio (80); // abbassa il braccio
       
          raccolta(); // raccoglie
      }
           
  }
  
  // Automatic release
  if(autom){  //se è stato preso un oggetto procedere a trasportarlo

      muovi_braccio(120);  // alza il braccio
      muovi_base(4); // ruotati verso il cestino

      delay(2000);
        
      degree_mano = 75; // apri la mano     
      servo_mano_1.write(degree_mano);
      servo_mano_2.write(180-degree_mano);
        
      delay(2000);
   
      muovi_base(90);  // ruotati nella posizione neutra (90°)
      
      autom = false; // disattiva il rilascio automatico
  }
  
 
  // stampe di controllo
  Serial.print("Base: ");
  Serial.print(degree_base);
  Serial.print(" - Braccio1: ");
  Serial.print(degree_braccio);
  Serial.print(" - Braccio2: ");
  Serial.println(180-degree_braccio);
  Serial.print(" - Mano1: ");
  Serial.print(degree_mano);
  Serial.print(" - Mano2: ");
  Serial.println(180-degree_mano);
  Serial.print(" - Switch1: ");
  Serial.println(digitalRead(SW_pin));
  Serial.print(" - Switch2: ");
  Serial.println(digitalRead(SW_mano_pin));
  Serial.println("\n");
  Serial.print(" - Distance: ");
  Serial.println(distance);
  Serial.println("\n");
  // fine stampe di controllo

  delay(50);
}

// funzione che muove la base fino a raggiungere i gradi inseriti in input
void muovi_base (int val){  // dato il valore in gradi muove la base fino ad es
  if (degree_base < val){
      while(degree_base <= val){
          degree_base = degree_base+1;
          servo_base.write(degree_base);
          delay(50);
      }
  }
  else{
      while(degree_base >= val){
              degree_base = degree_base-1;
              servo_base.write(degree_base);
              delay(50);
      }
  }
}

// funzione che muove il braccio fino a raggiungere i gradi inseriti in input
void muovi_braccio (int val){
  if (degree_braccio < val){
      while(degree_braccio <= val){
          degree_braccio = degree_braccio + myspeed;
          servo_braccio_1.write(degree_braccio);
          servo_braccio_2.write(180-degree_braccio);
          delay(50);
      }
  }
  else{
      while(degree_braccio >= val){
              degree_braccio = degree_braccio - myspeed;
              servo_braccio_1.write(degree_braccio);
              servo_braccio_2.write(180-degree_braccio);
              delay(50);
      }
  }
}

// funzione che stringe la mano fino a che il microswitch non avverte del riuscimento della presa opppure una volta stretto del tutto del fallimento
void raccolta (){
  while(digitalRead(SW_mano_pin)==1 && degree_mano <= 130){
                  degree_mano = degree_mano + 2;
                  servo_mano_1.write(degree_mano);
                  servo_mano_2.write(180-degree_mano);
                  delay(50);
                }
              //se non prende torna nella posizione di default  
              if (degree_mano >= 130){
                degree_mano = 65;
                servo_mano_1.write(degree_mano);
                servo_mano_2.write(180-degree_mano);
               }
               //stringi un po' per migliorare la presa
              else{
                degree_mano = degree_mano + 4; // stringe ulteriormente l'oggetto per afferrarlo meglio
                servo_mano_1.write(degree_mano);
                servo_mano_2.write(180-degree_mano);
                autom = true;
              }
}
